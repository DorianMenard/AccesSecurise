#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <cstdlib>
#include <fstream>


#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <iostream>

#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

using namespace std;

int main()
{
    system ("dir /var/lib/bluetooth > test.txt");
    int envoie = 0;
    ifstream fichier("/home/pi/Desktop/Client_Rasp/ClientRaspberry/test.txt", ios::in);
    string AddBlt = "B8:27:EB:E2:F1:6A";
    string mot;
    while(fichier >> mot)
    {
        cout << mot << endl;
        if(mot == AddBlt)
            envoie = 1;
    }
    fichier.close();

    struct sockaddr_rc addr = { 0 };
    int s, status;
    char dest[18] = "00:19:5B:52:7D:61";

    // allocate a socket
    s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    // set the connection parameters (who to connect to)
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) (1&0xFF);
    str2ba( dest, &addr.rc_bdaddr );

    // connect to server
    status = connect(s, (struct sockaddr *)&addr, sizeof(addr));
    // send a message
    if( status == 0 ) {
        cout << "test" << endl;
        if(envoie == 1)
            status = write(s, "B8:27:EB:E2:F1:6A", 17);
        else
            cout << "Le bluetooth de la Raspberry n'est pas activer" << endl;
    }

    if( status < 0 ) perror("uh oh");

    close(s);
    return 0;
}
