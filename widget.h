#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <iostream>
//#include <uuids.h>
//#include <guiddef.h>
#include <initguid.h>
#include <WinSock2.h>
#include <ws2bth.h>
#include <bthsdpdef.h>
#include <BluetoothAPIs.h>
#include <Windows.h>
#include <strsafe.h>
#include <lmerr.h>
#include <winerror.h>
//#include <ks.h>
#include <string>
#include <climits>
#include <cstdlib>
#include <string>
#include <locale>
#include <cstring>

#include <Iphlpapi.h>
#include <Assert.h>

using namespace std;
inline int modulo (int m, int n)
{
    return m >= 0 ? m % n : ( n - abs ( m % n ) ) % n;
}

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void Cons();
    char* getMAC();

    //Client
    Connexion(char * , int);
    Envoyer(char *chaine);
    Recevoir(int);

    //Serv
    void test();
    bool envoyer();
    bool recevoir();


private slots:
    void on_pushButton_clicked();

private:
    Ui::Widget *ui;
    DWORD monthread2;
    SOCKET socket_travail2;
    DWORD WINAPI ClientThread2(LPVOID p);

    //Client
    WSADATA WSAData;
    SOCKET id_socket;
    int tempo;
    SOCKADDR_IN information_dest;
    int nb_caract2;
    char buffer[1500];
    int erreur;
    char chaine[15];

    //Serv
    WSADATA wsd;
    SOCKADDR_BTH sa;
    SOCKET server_socket;
    SOCKET s2;
    char Sbuffer[1500];
    int nb_caract;
    int erreur2;
    DWORD monthread;
    SOCKET socket_travail;
};

#endif // WIDGET_H
