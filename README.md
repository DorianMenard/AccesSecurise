# AccesSecurise

Contexte : Contrôler l'accès vers un espace sécurisé matérialisé par la commande de l'ouverture d'une porte équipée d'une gâche électrique

Partie Bluetooth :

Pour cette partie il y a un Client Bluetooth sous raspberry qui est le fichier Raspberry.cpp. Ce fichier communique avec le serveur Bluetooth qui se trouve dans le fichier Widget.cpp.
Le Fichier Widget.cpp récupére les informations du Client Bluetooth pour ensuite les envoyer en Tcp/Ip à un serveur TCP(Ce serveur va servir à ouvrir une gâche électrique).

-Fichier Raspberry.cpp :

    Ce fichier est le Client Bluetooth il va permettre d'obtenir l'adresse Mac Bluetooth de la Raspberry pour ensuite l'envoyer à un serveur Bluetooth.
    
-Fichier Widget.h :

    Ce fichier est le fichier d'en-tête du Serveur Bluetooth. Il est composé d'une class qui va servir à faire pour avoir des méthodes pour le Serveur Bluetooth et d'autre méthodes
    pour le Client Tcp/Ip qui communique avec un serveur TCP.
    
-Fichier Widget.cpp

    Ce fichier contient toute les méthodes Serveur/Client :
        
        -Méthode GetMac : Permet d'obtenir l'adresse Mac de la machine
        -Méthode test : Permet d'accepter les connections reçus par le Serveur Bluetooth
        -Méthode recevoir : Permet de recevoir le message envoyé par le Client Bluetooth puis de les afficher
        -Méthode envoyer : Permet de renvoyer une réponse au Client Bluetooth pour confirmer la bonne réception des données, puis d'appeler les Méthodes Connexion et Envoyer.
        -Méthode Connexion : Permet de se connecter à un serveur TCP grâce a son adresse Ip et son numéro de Port.
        -Méthode Envoyer : Permet de crypter les données grâce au code César puis de les envoyer en TCP/IP au serveur et de recevoir une réponse.
        
